# Scripts

Scripts I use to configure my systems.

**Audio**  
To prevent Topping E30 USB DAC going to powersave mode and creating crackle sounds doing so, put usb-powersave.rules into /etc/udev/rules.d
