# Setup

use manual partioning to setup os disk:
1. 500MB efi partition mounted on /boot/efi
2. 1024MB boot partition formated as ext4 on /boot
3. root partition formated as encrypted btrfs

create subvolumes on root partition:
1. root mounted on /
2. home mounted on /home
3. snapshots mounted on /.snapshots
4. opt mounted on /opt
5. tmp mounted on /tmp
6. usr-local mounted on /usr/local
7. var mounted on /var

# post-install Config

Update System
```sh
sudo dnf up
flatpak update
```

## btrfs Snapshots

check top level subvolumes
```sh
sudo btrfs subvolume list / | grep "level 5"
```

install snapper
```sh
sudo dnf install snapper python3-dnf-plugin-snapper
```

### configure snapshots for root subvolume

#### snapper setup

unmount the created snapshot subvolume
```sh
sudo umount /.snapshots
sudo rmdir /.snapshots
```

ceate snapper config for root, delete nesteed snapshots subvolume and recreate snapshots directory
```sh
sudo snapper -c root create-config /
sudo btrfs subvolume list /
sudo btrfs subvolume delete /.snapshots
sudo mkdir /.snapshots
```

reload fstab and automount
```sh
sudo systemctl daemon-reload
sudo mount -a
```

update root volume id to / subvolume
```sh
sudo btrfs subvolume get-default /
sudo btrfs subvolume list / | grep "level 5" #remember ID of root
sudo btrfs subvolume set-default rootID / # set rootID to the id of root
sudo grubby --update-kernel=ALL --remove-args="rootflags=subvol=root"
```

#### Make snapshots bootable

update efi config
```sh
sudo nano /boot/ef/EFI/fedora/grub.cfg
# Add at first line: set btrfs_relative_path="yes"
```

update grub config
```sh
sudo echo 'USE_BTRFS_SNAPSHOT_BOOTING="true"' >> /etc/default/grub
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo snapper -c root set-config SYNC_ACL=yes ALLOW_USERS=$USER
sudo chown -R :$USER /.snapshots
sudo grub2-editenv - unset menu_auto_hide
```

install grub-btrfs utility
```sh
git clone https://github.com/Antynea/grub-btrfs.git
cd /grub-btrfs
sudo make install
```

edit grub-btrfs config
```sh
sudo nano /etc/default/grub-btrfs/config
# uncomment line: GRUB_BTRFS_SHOW_TOTAL_SNAPSHOTS_FOUND="true"
# uncomment line: GRUB_BTRFS_GRUB_DIRNAME="/boot/grub2"
# uncomment line: GRUB_BTRFS_MKCONFIG=/usr/bin/grub2-mkconfig and change bin to sbin
# uncomment line: GRUB_BTRFS_SCRIPT_CHECK=grub2-script-check
```

update grub
```sh
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
```

enable grub-btrfs service daemon
```sh
sudo systemctl start grub-btrfsd
sudo systemctl enable grub-btrfsd
```

### configure snapshots for home subvolume

configure snapshots for home subvolume
```sh
sudo snapper -c home create-config /home
sudo snapper -c home set-config SYNC_ACL=yes ALLOW_USERS=$USER
sudo systemctl enable --now snapper-timeline.timer
sudo systemctl enable --now snapper-cleanup.timer
```

configure sheduled snapshots
```sh
sudo nano /etc/snapper/configs/root #set "TIMELINE_CREATE="no""
sudo snapper -c home set-config SYNC_ACL=yes ALLOW_USERS=$USER
```

## Filesystem

filesystem optimizations
```
# add to fstab:
ssd,noatime,commit=120,compress=zstd
```

check if trim is passed through luks
```sh
sudo cat /etc/crypttab
# check for 'discard' option at end of line
```

enable ssd trim
```sh
sudo systemctl enable fstrim.timer
```

### Ganes Disk - encrypted btrfs

this part is mostly taken from: https://gist.github.com/MaxXor/ba1665f47d56c24018a943bb114640d7

Create keyfile:
```sh
dd bs=64 count=1 if=/dev/urandom of=/etc/cryptkey iflag=fullblock
chmod 600 /etc/cryptkey
```

Encrypt devices:
```sh
cryptsetup -v -c aes-xts-plain64 -h sha512 -s 512 luksFormat /dev/sda /etc/cryptkey
```

Backup LUKS header:
```sh
cryptsetup luksHeaderBackup --header-backup-file ~/sda.header.bak /dev/sda
```

Automatically unlock LUKS devices on boot by editing `/etc/crypttab`:
```sh
games UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /etc/cryptkey luks,noearly,discard
# Use 'blkid /dev/sdb' to get the UUID
```

Unlock encrypted devices now to create the filesystem in next step:
```sh
cryptsetup open --key-file=/etc/cryptkey --type luks /dev/sda games
```

Create filesystem:
```sh
mkfs.btrfs -L games /dev/mapper/games
```

Mount filesystem:
```sh
mount -t btrfs -o defaults,ssd,noatime,compress=zstd /dev/mapper/games /mnt/tmp
```

Create subvolumes and umount:
```sh
btrfs subvolume create /mnt/tmp/games_mid
umount /mnt/tmp
```

Create direcotries to mount subvolumes
```sh
mkdir /mnt/games_mid
```

Automatically mount subolvumes on boot by editing `/etc/fstab`:
```sh
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx      /mnt/games_mid          btrfs   subvol=games_mid,ssd,noatime,commit=120,discard=async,compress=zstd:1,noauto,x-systemd.automount,x-systemd.device-timeout=10min 0       2
/mnt/games_mid                                  /home/USER/Games/mid  none    defaults,bind                                                                                                                   0       0
```

### Data Disks - encrypted btfs raid1

#### Initial setup with LUKS/dm-crypt

Encrypt devices:
```sh
cryptsetup -v -c aes-xts-plain64 -h sha512 -s 512 luksFormat /dev/sdb /etc/cryptkey
cryptsetup -v -c aes-xts-plain64 -h sha512 -s 512 luksFormat /dev/sdc /etc/cryptkey
```

Backup LUKS header:
```sh
cryptsetup luksHeaderBackup --header-backup-file ~/sdb.header.bak /dev/sdb
cryptsetup luksHeaderBackup --header-backup-file ~/sdc.header.bak /dev/sdc
```

Automatically unlock LUKS devices on boot by editing `/etc/crypttab`:
```sh
data1 UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /etc/cryptkey luks,noearly #,discard (for SSDs)
data2 UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx /etc/cryptkey luks,noearly #,discard (for SSDs)
# Use 'blkid /dev/sdb' to get the UUID
```

Unlock encrypted devices now to create the filesystem in next step:
```sh
cryptsetup open --key-file=/etc/cryptkey --type luks /dev/sdb data1
cryptsetup open --key-file=/etc/cryptkey --type luks /dev/sdc data2
```

Create filesystem:
```sh
mkfs.btrfs -m raid1 -d raid1 /dev/mapper/data1 /dev/mapper/data2
```

Mount filesystem:
```sh
mount -t btrfs -o defaults,noatime,compress=zstd /dev/mapper/data1 /mnt/tmp
```

Create subvolumes and umount:
```sh
btrfs subvolume create /mnt/tmp/nextcloud
btrfs subvolume create /mnt/tmp/games_slow
umount /mnt/tmp
```

Create direcotries to mount subvolumes
```sh
mkdir /mnt/nextclooud
mkdir /mnt/games_slow
```

Automatically mount subolvumes on boot by editing `/etc/fstab`:
```sh
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx      /mnt/nextcloud          btrfs   subvol=nextcloud,nossd,noatime,compress=zstd                                                                                    0       2
/mnt/nextcloud                                  /home/USER/Nextcloud  none    defaults,bind                                                                                                                   0       0       
UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx    /mnt/games_slow         btrfs   subvol=games_slow,nossd,noatime,compress=zstd                                                                                   0       2
/mnt/games_slow                                 /home/USER/Games/slow none    defaults,bind                                                                                                                   0       0 
# Add option 'autodefrag' to allow automatic defragmentation: useful for files with lot of random writes like databases or virtual machine images
```

#### Maintenance

In a btrfs raid setup it is necessary to frequently run a `btrfs scrub` to check for corrupted blocks/flipped bits and repair them using a healthy copy from one of the mirror disks.

In the example below a systemd-timer is used to run an automatic `btrfs scrub` job each month.

/etc/systemd/system/btrfs-scrub.timer:
```
[Unit]
Description=Monthly scrub btrfs filesystem, verify block checksums
Documentation=man:btrfs-scrub

[Timer]
# first saturday each month
OnCalendar=Sat *-*-1..7 3:00:00
RandomizedDelaySec=10min

[Install]
WantedBy=timers.target
```

/etc/systemd/system/btrfs-scrub.service:
```
[Unit]
Description=Scrub btrfs filesystem, verify block checksums
Documentation=man:btrfs-scrub

[Service]
Type=simple
ExecStart=/bin/btrfs scrub start -Bd /mnt/data
KillSignal=SIGINT
IOSchedulingClass=idle
CPUSchedulingPolicy=idle
```

## Packetmanagement

speedup dnf
```sh
echo 'fastestmirror=1' | sudo tee -a /etc/dnf/dnf.conf
echo 'max_parallel_downloads=10' | sudo tee -a /etc/dnf/dnf.conf
echo 'deltarpm=true' | sudo tee -a /etc/dnf/dnf.conf
cat /etc/dnf/dnf.conf
```

add 3rd party repos in system settings

add flathub as flatpack repo
```sh
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update
```

## System

set hostname
```sh
hostnamectl set-hostname $hostname
```

## Audio

copy alsa config to wireplumber
```sh
sudo cp -a /usr/share/wireplumber/main.lua.d/50-alsa-config.lua /etc/wireplumber/main.lua.d/50-alsa-config.lua
```

disable pipewire suspend
```sh
sudo nano /etc/wireplumber/main.lua.d/50-alsa-config.lua #set session.suspend-timout-seconds to 0
systemctl --user restart wireplumber
```

configure sample rates
```sh
sudo nano /etc/wireplumber/main.lua.d/50-alsa-config.lua # set audio.rate to 48000; set audio.allowed-rates to 44100,88200,96000,176400,192000,352800,384000,705600,768000
```

configure audio format
```sh
sudo nano /etc/wireplumber/main.lua.d/50-alsa-config.lua # set audio.format to s32le
```

add udev rule for USB DAC
```sh
echo 'ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="152a", ATTR{idProduct}=="8750", TEST=="power/control", ATTR{power/control}="on"' >> /etc/udev/rules.d/usb-power.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
```

## Nitrokey

install nitropy
```sh
pip install pynitrokey
```

add udev rule
```sh
wget https://raw.githubusercontent.com/Nitrokey/libnitrokey/master/data/41-nitrokey.rules
sudo mv 41-nitrokey.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules && sudo udevadm trigger
```

configure pam to use nitrokey for login and sudo
```sh
sudo dnf install pam-u2f
mkdir ~/.config/Nitrokey
pamu2fcfg > ~/.config/Nitrokey/u2f_keys
sudo mv ~/.config/Nitrokey /etc

```

## AMD PSTATE Settings
WIP

#
