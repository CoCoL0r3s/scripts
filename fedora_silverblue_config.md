# Introduction

This is my personal Fedora Silverblue Configuration. Feel free to use what ever aids you.

I am running this on a Desktop PC with 4 drives:

- 1TB NVME SSD: used as system drive and for games which need a fast drive to not load for ages
- 512GB SATA SSD: my old system drive, now used for games
- 2x 2TB HDDs: used for data as RAID1

# Setup

**Warning**: It is not advised by fedora devs to use manual partitioning with silverblue, I do it nethertheless and so far it is working fine.
Further reference: https://docs.fedoraproject.org/en-US/fedora-silverblue/installation/

## system drive

use manual partioning to setup os disk:
1. 500MB efi partition mounted on /boot/efi
2. 1024MB boot partition formated as ext4 on /boot
3. root partition formated as encrypted (luks2) btrfs (no mountpoint)

create subvolumes on btrfs volume:
1. root mounted on /
2. home mounted on /var/home
3. gaming_fast mounted on /var/mnt/games_fast

## gaming drive

use manual partioning to setup gaming disk:
1. create encrypted (luks2) btrfs volume without mountpoint

create subvolumes on btrfs volume:
1. games_mid mounted on /var/mnt/games_mid

## data drives

use manual partioning to setup gaming disk:
1. create encrypted (luks2) raid1 btrfs volume without mountpoint

create subvolumes on btrfs volume:
1. games_slow mounted on /var/mnt/games_slow
2. nextcloud mounted on /var/mnt/nextcloud

# Post install

## First things first

Update base System
```sh
flatpak update
sudo rpm-ostree upgrade
systemctl reboot
```

Add and enable flathub repository
```sh
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update
```

set hostname
```sh
hostnamectl set-hostname $hostname
```

## fstab

Anaconda doesn't set ideal options so I add some more. Further I am using --bind mounts to access drive mountpoints from home directory. 
Remember to create the directorys to bind mount first, like in this example:
```sh
mkdir ~/Nextcloud
chown $USER:$USER Nextcloud
```

```sh
sudo nano /etc/fstab
```
- add to ssd drive options: ssd,noatime,commit=120
- add to hdd drive options: nossd,noatime
- set fsck from / mount to 1
- set fsck from all other btrfs subvolumes to 2
- add all bind mounts like this example:
```
/var/mnt/nextcloud /var/home/$USER/Nextcloud none defaults,bind 0 0
```
now reload the fstab and automount
```sh
sudo systemctl daemon-reload
sudo mount -a
```

## ssd trim

enable ssd trim
```sh
sudo systemctl enable fstrim.timer
```

## btrfs raid settings

In a btrfs raid setup it is necessary to frequently run a `btrfs scrub` to check for corrupted blocks/flipped bits and repair them using a healthy copy from one of the mirror disks.

In the example below a systemd-timer is used to run an automatic `btrfs scrub` job each month.

/etc/systemd/system/btrfs-scrub.timer:
```
[Unit]
Description=Monthly scrub btrfs filesystem, verify block checksums
Documentation=man:btrfs-scrub

[Timer]
# first saturday each month
OnCalendar=Sat *-*-1..7 3:00:00
RandomizedDelaySec=10min

[Install]
WantedBy=timers.target
```

/etc/systemd/system/btrfs-scrub.service:
```
[Unit]
Description=Scrub btrfs filesystem, verify block checksums
Documentation=man:btrfs-scrub

[Service]
Type=simple
ExecStart=/bin/btrfs scrub start -Bd /mnt/data
KillSignal=SIGINT
IOSchedulingClass=idle
CPUSchedulingPolicy=idle
```

## Audio

I am using an external DAC (Topping E30). Some changes to pipewire and wireplumber configs have to be made to unlock the full potential of this DAC.

## wireplumber

copy wireplumber alsa config to /etc/wireplumber
```sh
sudo cp -a /usr/share/wireplumber/main.lua.d/50-alsa-config.lua /etc/wireplumber/main.lua.d/50-alsa-config.lua
```

disable suspend
```sh
sudo nano /etc/wireplumber/main.lua.d/50-alsa-config.lua 
#set session.suspend-timout-seconds to 0
systemctl --user restart wireplumber
```

configure sample rates
```sh
sudo nano /etc/wireplumber/main.lua.d/50-alsa-config.lua 
# set audio.rate to 48000; set audio.allowed-rates to 44100,48000,88200,96000,176400,192000,352800,384000,705600,768000
```

configure audio format
```sh
sudo nano /etc/wireplumber/main.lua.d/50-alsa-config.lua 
# set audio.format to s32le
```

reload wireplumber
```sh
systemctl --user restart wireplumber
```

## pipewire

copy pipewire configs to /etc/pipewire
```sh
sudo cp -R /usr/share/pipewire /etc/pipewire
```

disable  suspend
```sh
sudo nano /etc/pipewire/pipewire.conf 
#set session.suspend-timout-seconds to 0
systemctl --user restart wireplumber
```

configure sample rates
```sh
sudo nano /etc/pipewire/pipewire.conf 
# set audio.rate to 48000; set audio.allowed-rates to [ 44100 48000 88200 96000 176400 192000 352800 384000 705600 768000 ]
```

configure audio format
```sh
sudo nano /etc/pipewire/pipewire.conf 
# set audio.format to s32le
```

set resampling quality (should not be necessary as all sample rates are set)
```sh
sudo nano /etc/pipewire/client.conf 
# set resample.quality = 10
sudo nano /etc/pipewire/pipewire-pulse.conf
# set resample.quality = 10
```

reload pipewire
```sh
systemctl --user restart pipewire.service pipewire-pulse.socket
```

## udev rule

To prevent the DAC from going in standby I add a udev rule:
add udev rule for USB DAC
```sh
echo 'ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="152a", ATTR{idProduct}=="8750", TEST=="power/control", ATTR{power/control}="on"' >> /etc/udev/rules.d/usb-power.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
```

# Nitrokey Support

install nitropy
```sh
pip install pynitrokey
```

add udev rule
```sh
wget https://raw.githubusercontent.com/Nitrokey/libnitrokey/master/data/41-nitrokey.rules
sudo mv 41-nitrokey.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules && sudo udevadm trigger
```

# Software

## RStudio on toolbox

```sh
toolbox create --release f37 R
toolbox enter R
sudo dnf install rstudio-desktop
sudo dnf install 'dnf-command(copr)'
sudo dnf copr enable iucar/cran
sudo dnf install R-CoprManager
```
